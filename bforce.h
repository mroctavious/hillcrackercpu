#include "trie.c"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>
#include <stdint.h>
#define MODULE 41
FILE *FILE_RES;
typedef struct _thrdArgs
{
	Trie *suffixTree;
	TrieValue true;
	int module;
	int size;
	char str[1000];
	int thrdId;
} thrdArgs;


//char ALPHABET[]="0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ"; //43
//char ALPHABET[]="0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//char ALPHABET[]="0123456789<=>?ABCDEFGHIJKLMNOPQRSTUVWXYZ "; //41
//char MESSAGE[]="HOLA COMO ESTAS";
//char   MESSAGE[]="BV AY2T4LZSNCE5";
//char ALPHABET[]="0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//#define ALPHA_LEN 41
char ALPHABET[]="123456789@AEIOUabcdefghijklmnopqrstuvwxyz";
#define ALPHA_LEN 41
char MESSAGE[]="feuqrgo@wz3(6hIImfzowkIadv62oac3";
int keyIndexChar[256];
int msgSize;
char keyIndex[ALPHA_LEN];

//Create the mapping index
void getKeyIndex( int msgLen );

//Get int from char
int getInt( char ltr );

//Get the char from mapping
char getChar( int index );

//Get the power of a number
int ipow(int base, int exp);

//Funcion que aplica la matriz A a la llave B
void matrixMult(int *C, int *A, int *B, int N );

//Convierte en minusculas un string
void strToLower(char *str);

//Function which will return if a string is found or not
int isFound(TrieValue res);

//Verificar si existe la palabra en el arbol de sufijos
void suffixVerify(char *str, thrdArgs *args );

//Funcion que carga la lista de palabras gringas :/
short int loadFile(Trie **suffixTree, char *wordFile);

//Recursive function to verify the base mod
int verifyBase(int *key, int mod, int k, int size, int *flag);

//Print a square matrix
void printM(int *key, int n);

//Apply the created key to the alphabet
void applyKey(int *key, char *dec, int size);

//Create a new key sequence
void createKeysSeq(int *key, int size, int mod, thrdArgs *args);

//Aplicar fuerzabruta usando varios hilos
void *parallelSearch(void *args);


#include "bforce.c"
