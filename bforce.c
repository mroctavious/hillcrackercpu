void getKeyIndex( int msgLen )
{
	int i;
	for( i=0; i<strlen(ALPHABET); i++ )
	{
		keyIndexChar[ ALPHABET[i] ] = i ;
		keyIndex[i]=ALPHABET[i];
		//printf("int:%d Char:%d\t", keyIndexChar[ ALPHABET[i] ], keyIndex[i]=ALPHABET[i]);
	}
	printf("\n");
}


int getInt( char ltr )
{
	return keyIndexChar[ltr];
}

char getChar( int index )
{
	return keyIndex[index];
}


int ipow(int base, int exp)
{
	int result = 1;
	for (;;)
	{
		if (exp & 1)
			result *= base;
		exp >>= 1;
		if (!exp)
			break;
		base *= base;
	}
	return result;
}

//Funcion que aplica la matriz A a la llave B
void matrixMult(int *C, int *A, int *B, int N )
{
	//Avoiding trash
	memset(C, 0, sizeof(int) * N);

	int i,j;
	//Multiply the key
	for(i = 0; i < N; ++i)
	{
		for(j = 0; j < N; ++j)
		{
				C[i] += A[i*N+j] * B[j];
		}
	}
}


//Convierte en minusculas un string
void strToLower(char *str)
{
	short int i=0;
	short int len=strlen(str);
	//Recorre todas las letras y las convierte en minusculas
	//lo guarda en el mismo arreglo de entrada
	while( i < len )
	{
		str[i]=tolower(str[i]);
		++i;
	}
}
int isFound(TrieValue res)
{
	//Verificar si encontro el resultado
	if( res == NULL )
		//printf("Not found!\n");
		return 0;
	else
		//printf("%s\n", res);
		return 1;
}
//Verificar si existe la palabra en el arbol de sufijos
void suffixVerify(char *str, thrdArgs *args )
{
	const char delimiter[2] = " ";
	char *token;
	int tries=0;
	token=strtok( str, delimiter );
	while( token != NULL )
	{
		strToLower(token);
		TrieValue res=trie_lookup( args->suffixTree, token );
		if( isFound(res) )
		{
			//Add to file
			//printf("Encontrado");
			fprintf( FILE_RES, "%s\n", str);
		}
		token = strtok( NULL, delimiter );
	}
}

//Funcion que carga la lista de palabras gringas :/
short int loadFile(Trie **suffixTree, char *wordFile)
{
	TrieValue true="1";

	//Intentamos abrir el archivo
	FILE *file = fopen(wordFile, "r");

	//Verificamos si lo pudo abrir, si no mostrar error
	if( file == NULL )
	{
		fprintf(stderr, "Cannot open file: %s\n", wordFile);
		return 1;
	}

	//Reservamos memoria necesaria para leer archivo
	size_t buffSize = 100;
	char *buffer=(char*)malloc( sizeof(char) * buffSize );
	memset(buffer,'\0', 100);
	size_t size;
	int line_number = 0;

	//Se recorre todo el archivo
	while( (size=getline(&buffer, &buffSize, file)) != -1 )
	{
		//Quitamos salto de linea, convertimos a minusculas,
		//e insertamos al arbol
		buffer[size-1]='\0';
		strToLower(buffer);
		trie_insert(*suffixTree, buffer, true);
	}

	fflush(stdout);
	fclose(file);
	free(buffer);
	return 0;
}

int verifyBase(int *key, int mod, int k, int size, int *flag)
{
	if( key[k] == mod )
	{
		key[k]=0;
		key[k+1]+=1;
		if( k+1 == size )
			*flag=1;
		else
			return verifyBase(key, mod, k+1, size, flag);
	}
	else
		return 0;
}

void printM(int *key, int n)
{
	int i,j;
	for( i=0; i<n; i++)
	{
		for( j=0; j<n; j++ )
		{
			printf("%d ", key[i*n+j]);
		}
		printf("\n");
	}
}
void applyKey(int *key, char *dec, int size)
{
	int i,j;
	int res[size];
	int tmpB[size];
	//Empezando a decriptar mensaje
	for(i=0; i < (msgSize/size); i++ )
	{
		//Conseguimos el mapeo de las letras
		for( j=0; j < size; j++)
		{
			tmpB[j]=getInt( MESSAGE[ i*size+j ] ); //B
			//printf("%c: ", getChar( tmpB[j] ) );
		}
		//Aplicamos la llave
		matrixMult( res, key, tmpB, size );

		//Aplicamos el modulo al resultado
		for(j=0; j<size; j++)
		{
			res[j]=res[j]%MODULE;
			//printf( "%d - ", res[j] );
		}
		for( j=0; j < size; j++)
		{
			dec[i*size+j]=getChar( res[j] );
		}
	}
}

void createKeysSeq(int *key, int size, int mod, thrdArgs *args)
{
	int flag=0;
	int i;
	char msg[msgSize+1];
	while( ! flag )
	{
		for(i=0; i<mod; i++)
		{
			//Probar llave
			applyKey(key, msg, size );

			//Buscar palabra en el arbol
			suffixVerify( msg, args );

			//Buscamos la palabra
			//printf("Phrase: %s\n ", msg);

			//Ir a la siguiente combinacion
			*key+=1;
		}
		verifyBase(key, mod, 0, size*size, &flag);
	}
}

//Cambiar a parallelBruteForce
void *parallelSearch(void *args)
{
	thrdArgs *myArgs=args;
	//printf("Modulo->%d\n n=%d\n", myArgs->module, myArgs->n);
	//TrieValue res=trie_lookup( myArgs->suffixTree, "liechtenstein" );
	//isFound(res);
	int size=myArgs->size;
	int key[size*size];
	int maxSize=(size*myArgs->thrdId)+size;
	memset(key,0,sizeof(int)*size*size);

	createKeysSeq( key, size, myArgs->module, args );
}
