#define MODULE 41

#include "bforce.h"

int main(int argc, char **argv)
{

	if( argc < 3 )
	{
		printf("Error de syntaxis!\nEjemplo:\n\t./program wordList.txt output.txt\n");
		exit(1);
	}

	//Conseguimos tamano de la palabra
	msgSize=strlen(MESSAGE);

	//Cargamos los indices
	getKeyIndex(msgSize);


	//printf("%s\n",dec);
	//Iniciamos arbol
	Trie *suffixTree=trie_new();

	//Creamos valores de verdadero y falso
	TrieValue true="1";
	TrieValue false="0";

	//Insertamos palabras al arbol de sufijos
	loadFile(&suffixTree, argv[1]);

	//Lanzamos hilo, retorna un numero entero
	//Creamos n hilos con su debida estruc
	pthread_t threads[4];
	thrdArgs args[4];

	//Configuracion de los hilos
	args[0].suffixTree=suffixTree;
	args[1].suffixTree=suffixTree;
	args[2].suffixTree=suffixTree;
	args[3].suffixTree=suffixTree;
	args[0].true=true;
	args[1].true=true;
	args[2].true=true;
	args[3].true=true;
	args[0].module=41;
	args[1].module=41;
	args[2].module=41;
	args[3].module=41;
	args[0].size=2;
	args[1].size=4;
	args[2].size=8;
	args[3].size=16;
	args[0].thrdId=0;
	args[1].thrdId=1;
	args[2].thrdId=2;
	args[3].thrdId=3;


	//Abrimos archivo donde se guardaran los resultados
	FILE_RES=fopen(argv[2], "w");
	if (FILE_RES == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}

	//Enviamos los hilos a ejecutarse
	int iret1 = pthread_create( &threads[0], NULL, parallelSearch, (void*) &args[0]);
	int iret2 = pthread_create( &threads[1], NULL, parallelSearch, (void*) &args[1]);
	int iret3 = pthread_create( &threads[2], NULL, parallelSearch, (void*) &args[2]);
	int iret4 = pthread_create( &threads[3], NULL, parallelSearch, (void*) &args[3]);

	//Esperamos a que terminen
	pthread_join( threads[1], NULL);
	pthread_join( threads[2], NULL);
	pthread_join( threads[3], NULL);
	pthread_join( threads[0], NULL);

	//Liberamos el arbol
	trie_free(suffixTree);
	fclose(FILE_RES);

	printf("TERMINOª\n");
	return 0;
}
