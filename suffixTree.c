#include "trie.c"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>
#include <stdint.h>
#define MODULE 41

typedef struct _thrdArgs
{
	Trie *suffixTree;
	TrieValue true;
	int module;
	int size;
	char str[1000];
	int thrdId;
} thrdArgs;

FILE *FILE_RES;

//char ALPHABET[]="0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ"; //43
//char ALPHABET[]="0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//char ALPHABET[]="0123456789<=>?ABCDEFGHIJKLMNOPQRSTUVWXYZ "; //41
//char MESSAGE[]="HOLA COMO ESTAS";
//char   MESSAGE[]="BV AY2T4LZSNCE5";
//char ALPHABET[]="0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//#define ALPHA_LEN 41
char ALPHABET[]="123456789@AEIOUabcdefghijklmnopqrstuvwxyz";
#define ALPHA_LEN 41
char MESSAGE[]="feuqrgo@wz3(6hIImfzowkIadv62oac3";

int keyIndexChar[256];
char keyIndex[ALPHA_LEN];
int msgSize;

void getKeyIndex( int msgLen )
{
	int i;
	for( i=0; i<strlen(ALPHABET); i++ )
	{
		keyIndexChar[ ALPHABET[i] ] = i ;
		keyIndex[i]=ALPHABET[i];
		//printf("int:%d Char:%d\t", keyIndexChar[ ALPHABET[i] ], keyIndex[i]=ALPHABET[i]);
	}
	printf("\n");
}


int getInt( char ltr )
{
	return keyIndexChar[ltr];
}

char getChar( int index )
{
	return keyIndex[index];
}


int ipow(int base, int exp)
{
	int result = 1;
	for (;;)
	{
		if (exp & 1)
			result *= base;
		exp >>= 1;
		if (!exp)
			break;
		base *= base;
	}
	return result;
}

//Funcion que aplica la matriz A a la llave B
void matrixMult(int *C, int *A, int *B, int N )
{
	//Avoiding trash
	memset(C, 0, sizeof(int) * N);

	int i,j;
	//Multiply the key
	for(i = 0; i < N; ++i)
	{
		for(j = 0; j < N; ++j)
		{
				C[i] += A[i*N+j] * B[j];
		}
	}
}


//Convierte en minusculas un string
void strToLower(char *str)
{
	short int i=0;
	short int len=strlen(str);
	//Recorre todas las letras y las convierte en minusculas
	//lo guarda en el mismo arreglo de entrada
	while( i < len )
	{
		str[i]=tolower(str[i]);
		++i;
	}
}
int isFound(TrieValue res)
{
	//Verificar si encontro el resultado
	if( res == NULL )
		//printf("Not found!\n");
		return 0;
	else
		//printf("%s\n", res);
		return 1;
}
//Verificar si existe la palabra en el arbol de sufijos
void suffixVerify(char *str, thrdArgs *args )
{
	const char delimiter[2] = " ";
	char *token;
	int tries=0;
	token=strtok( str, delimiter );
	while( token != NULL )
	{
		TrieValue res=trie_lookup( args->suffixTree, token );
		if(  isFound(res) )
		{
			//Add to file
			//printf("Encontrado");
			fprintf( FILE_RES, "%s\n", str);
		}
		token = strtok( NULL, delimiter );
	}
}

//Funcion que carga la lista de palabras gringas :/
short int loadFile(Trie **suffixTree, char *wordFile)
{
	TrieValue true="1";

	//Intentamos abrir el archivo
	FILE *file = fopen(wordFile, "r");

	//Verificamos si lo pudo abrir, si no mostrar error
	if( file == NULL )
	{
		fprintf(stderr, "Cannot open file: %s\n", wordFile);
		return 1;
	}

	//Reservamos memoria necesaria para leer archivo
	size_t buffSize = 100;
	char *buffer=(char*)malloc( sizeof(char) * buffSize );
	memset(buffer,'\0', 100);
	size_t size;
	int line_number = 0;

	//Se recorre todo el archivo
	while( (size=getline(&buffer, &buffSize, file)) != -1 )
	{
		//Quitamos salto de linea, convertimos a minusculas,
		//e insertamos al arbol
		buffer[size-1]='\0';
		strToLower(buffer);
		trie_insert(*suffixTree, buffer, true);
	}

	fflush(stdout);
	fclose(file);
	free(buffer);
	return 0;
}

int verifyBase(int *key, int mod, int k, int size, int *flag)
{
	if( key[k] == mod )
	{
		key[k]=0;
		key[k+1]+=1;
		if( k+1 == size )
			*flag=1;
		else
			return verifyBase(key, mod, k+1, size, flag);
	}
	else
		return 0;
}

void printM(int *key, int n)
{
	int i,j;
	for( i=0; i<n; i++)
	{
		for( j=0; j<n; j++ )
		{
			printf("%d ", key[i*n+j]);
		}
		printf("\n");
	}
}
void applyKey(int *key, char *dec, int size)
{
	int i,j;
	int res[size];
	int tmpB[size];
	//Empezando a decriptar mensaje
	for(i=0; i < (msgSize/size); i++ )
	{
		//Conseguimos el mapeo de las letras
		for( j=0; j < size; j++)
		{
			tmpB[j]=getInt( MESSAGE[ i*size+j ] ); //B
			//printf("%c: ", getChar( tmpB[j] ) );
		}
		//Aplicamos la llave
		matrixMult( res, key, tmpB, size );

		//Aplicamos el modulo al resultado
		for(j=0; j<size; j++)
		{
			res[j]=res[j]%MODULE;
			//printf( "%d - ", res[j] );
		}
		for( j=0; j < size; j++)
		{
			dec[i*size+j]=getChar( res[j] );
		}
	}
}

void createKeysSeq(int *key, int size, int mod, thrdArgs *args)
{
	int flag=0;
	int i;
	char msg[msgSize+1];
	while( ! flag )
	{
		for(i=0; i<mod; i++)
		{
			//Probar llave
			applyKey(key, msg, size );

			//Buscar palabra en el arbol
			suffixVerify( msg, args );

			//Buscamos la palabra
			//printf("Phrase: %s\n ", msg);

			//Ir a la siguiente combinacion
			*key+=1;
		}
		verifyBase(key, mod, 0, size*size, &flag);
	}
}

//Cambiar a parallelBruteForce
void *parallelSearch(void *args)
{
	thrdArgs *myArgs=args;
	//printf("Modulo->%d\n n=%d\n", myArgs->module, myArgs->n);
	//TrieValue res=trie_lookup( myArgs->suffixTree, "liechtenstein" );
	//isFound(res);
	int size=myArgs->size;
	int key[size*size];
	int maxSize=(size*myArgs->thrdId)+size;
	memset(key,0,sizeof(int)*size*size);

	createKeysSeq( key, size, myArgs->module, args );
}

int main(int argc, char **argv)
{
	//Matriz de ejemplo
	int n=3;
	int A[n*n];
	A[0]=17;
	A[1]=17;
	A[2]=5;
	A[3]=21;
	A[4]=18;
	A[5]=21;
	A[6]=2;
	A[7]=2;
	A[8]=19;
/*
	A[0]=34;
	A[1]=14;
	A[2]=36;
	A[3]=3;
	A[4]=27;
	A[5]=19;
	A[6]=22;
	A[7]=0;
	A[8]=18;
*/
	int B[n];
	B[0]=30;
	B[1]=60;
	B[2]=90;
	B[3]=120;

	int C[n];
	//A[0]+=1;
//	verifyBase(A, 40, 0);
	matrixMult(C, A, B, 4);


	//printf("Phrase: %s\n ", msg);
	int i;
//	for(i=0; i<4; i++)
//	{
//		printf("%d  ", C[i]);
//	}
//	printf("\n");
///*************************************//

	printM(A, 3);

	for( i=0; i<strlen(ALPHABET); i++ )
	{
//		printf("int:%d Char:%d\n", keyIndexChar[ ALPHABET[i] ], keyIndex[i]=ALPHABET[i]);
	}


	//Conseguimos tamano de la palabra
	msgSize=strlen(MESSAGE);
	getKeyIndex(msgSize);
	//char dec[msgSize];
	//applyKey( A, dec, 3);


	//printf("%s\n",dec);
	//Iniciamos arbol
	Trie *suffixTree=trie_new();

	//Creamos valores de verdadero y falso
	TrieValue true="1";
	TrieValue false="0";

	//Insertamos palabras al arbol de sufijos
	loadFile(&suffixTree, argv[1]);

	//Lanzamos hilo, retorna un numero entero
	//Creamos n hilos con su debida estruc
	pthread_t threads[4];
	thrdArgs args[4];
	args[0].suffixTree=suffixTree;
	args[1].suffixTree=suffixTree;
	args[2].suffixTree=suffixTree;
	args[3].suffixTree=suffixTree;
	args[0].true=true;
	args[1].true=true;
	args[2].true=true;
	args[3].true=true;
	args[0].module=41;
	args[1].module=41;
	args[2].module=41;
	args[3].module=41;
	args[0].size=2;
	args[1].size=4;
	args[2].size=8;
	args[3].size=16;
	args[0].thrdId=0;
	args[1].thrdId=1;
	args[2].thrdId=2;
	args[3].thrdId=3;


	//Abrimos archivo donde se guardaran los resultados
	FILE_RES=fopen(argv[2], "w");
	if (FILE_RES == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}

	int iret1 = pthread_create( &threads[0], NULL, parallelSearch, (void*) &args[0]);
/*	int iret2 = pthread_create( &threads[1], NULL, parallelSearch, (void*) &args[1]);
	int iret3 = pthread_create( &threads[2], NULL, parallelSearch, (void*) &args[2]);
	int iret4 = pthread_create( &threads[3], NULL, parallelSearch, (void*) &args[3]);
	pthread_join( threads[1], NULL);
	pthread_join( threads[2], NULL);
	pthread_join( threads[3], NULL);
*/
	pthread_join( threads[0], NULL);
	//Liberamos el arbol
	trie_free(suffixTree);
	fclose(FILE_RES);
	//createKeysSeq(3, 20, 0);

	return 0;
}
